<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 01. 24.
 * Time: 9:32
 */

namespace Themaholic\CommonBundle\Doctrine;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;


/**
 * DateFormatFunction ::= "DATE_FORMAT" "(" ArithmeticPrimary "," StringPrimary ")"
 */
class DateFormatFunction extends FunctionNode
{
    // (1)
    public $firstDateExpression = null;
    public $formatExpression = null;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER); // (2)
        $parser->match(Lexer::T_OPEN_PARENTHESIS); // (3)
        $this->firstDateExpression = $parser->ArithmeticPrimary(); // (4)
        $parser->match(Lexer::T_COMMA); // (5)
        $this->formatExpression = $parser->StringPrimary(); // (6)
        $parser->match(Lexer::T_CLOSE_PARENTHESIS); // (3)
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'DATE_FORMAT(' .
        $this->firstDateExpression->dispatch($sqlWalker) . ', ' .
        $this->formatExpression->dispatch($sqlWalker) .
        ')'; // (7)
    }

} 