<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.06.10.
 * Time: 20:32
 */

namespace Themaholic\CommonBundle\Event;


class SymfonyEventDispatcherHandler implements EventHandlerInterface
{
    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    private $handler;

    public function __construct(\Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher)
    {
        $this->handler = $dispatcher;
    }

    public function handleEvent(EventInterface $event)
    {
        return $this->handler->dispatch($event->getEventName(), $event);
    }

} 