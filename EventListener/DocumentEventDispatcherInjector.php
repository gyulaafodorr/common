<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.02.02.
 * Time: 21:19
 */

namespace Themaholic\CommonBundle\EventListener;

use Themaholic\CommonBundle\Entity\BaseDomainEntity;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;

class DocumentEventDispatcherInjector
{
    /**
     * @var
     */
    private $eventDispatcher;

    /**
     * @param $dispatcher
     */
    public function __construct($dispatcher)
    {
        $this->eventDispatcher = $dispatcher;
    }

    /**
     * @param LifecycleEventArgs $arg
     */
    public function postLoad(LifecycleEventArgs $arg)
    {
        if ($arg->getDocument() instanceOf BaseDomainEntity)
        {
            $arg->getDocument()->setEventDispatcher($this->eventDispatcher);
        }
    }
}