<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.03.24.
 * Time: 6:59
 */

namespace Themaholic\CommonBundle\EventListener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SetLocaleOnLoginEventListener
{
    /**
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $token = $event->getAuthenticationToken();
        $request = $event->getRequest();
        $session = $request->getSession();

        $locale = $token->getUser()->getLocale();
        $session->set('_locale', $locale);
        $request->setLocale($locale);
    }
} 